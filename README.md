# Zevar Galaxy

Our app is an e-catalogue which along with the designs also provides you with an estimate of the price of the product according to the current rates. We can assist you to get a customized design for which you can contact through the app.

# Screen Shots

![alt text](images/Screenshot_from_2021-07-23_12-15-15.png "Welcome Screen") ![alt text](images/Screenshot_from_2021-07-23_12-15-27.png "Login Screen") ![alt text](images/Screenshot_from_2021-07-23_12-15-49.png "Home Screen")







# Features:
01. Simple and easy interface
Easy to browse app, where you can sort and filter as per your needs. Find the pieces you like, share then with your friends to take a second opinion!
02. Categorized view
The pieces are categorised as you would find them in your favourite store. We want you to experience the same ease without leaving the comfort of your home.
03. Sorting and filtering by price or weight
Sort the jewellery the way you would ask your shopping assistant. Get results just as per your interests.
04. Favourite designs section to help you shortlist your jewellery
And that beloved corner of shortlisted designs is available here as well. You can shortlist your favourite pieces and access all of them in the favourites section!

# Google play store 

[Live app : Zevar Galaxy](https://play.google.com/store/apps/details?id=com.ps.jewellery)



# Developer information

Parkhya Solution Pvt. Ltd.

https://www.parkhya.com/ 


